package com.sda.prog2.w2.sobota.wzorce;

import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class Run2 {

    public static void main(String[] args) {
        String descriptionDay="";
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Warsaw"), Locale.GERMANY);

//        calendar.clear();
        calendar.set(Calendar.YEAR,    2020);
        calendar.set(Calendar.DAY_OF_MONTH,    29);
        calendar.set(Calendar.MONTH,    5);

        int day = calendar.get(Calendar.DAY_OF_WEEK);
        System.out.println(day);
        System.out.println(calendar.get(Calendar.YEAR) +" / "+ calendar.get(Calendar.MONTH)+" / "+ calendar.get(Calendar.DATE));

        switch (calendar.get(Calendar.DAY_OF_WEEK)){
            case Calendar.THURSDAY:
                descriptionDay= "Thursday";
                break;
            case Calendar.FRIDAY:
                descriptionDay= "Friday";
                break;
            case Calendar.SATURDAY:
                descriptionDay= "Saturday";
                break;
            case Calendar.SUNDAY:
                descriptionDay= "Sunday";
                break;
            case Calendar.MONDAY:
                descriptionDay= "Monday";
                break;
            case Calendar.TUESDAY:
                descriptionDay= "Tuesday";
                break;
            case Calendar.WEDNESDAY:
                descriptionDay= "Wednesday";
                break;
        }
        System.out.println(descriptionDay);

        System.out.println(calendar.getCalendarType());

        calendar.clear();
        calendar.set(2020,5,4);
        System.out.println(calendar.get(Calendar.YEAR) +" / "+ calendar.get(Calendar.MONTH)+" / "+ calendar.get(Calendar.DATE));
        System.out.println("calendar.DAY_OF_WEEK dla poniedzialku 4V "+calendar.DAY_OF_WEEK); //powinno byc 2
    }
}
