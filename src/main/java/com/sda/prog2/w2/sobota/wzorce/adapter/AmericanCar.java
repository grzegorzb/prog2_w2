package com.sda.prog2.w2.sobota.wzorce.adapter;

public class AmericanCar implements AmericanMovable{
    double speed;

    public double getSpeedKMH() {
        return speed * SpeedConverter.MILES_TO_KILOMETERS.getConverter();
    }

    @Override
    public double getSpeed() {
        return getSpeedKMH();
    }
}
