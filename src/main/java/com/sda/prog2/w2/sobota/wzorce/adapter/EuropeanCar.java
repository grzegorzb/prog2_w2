package com.sda.prog2.w2.sobota.wzorce.adapter;

public class EuropeanCar implements EuropeanMovable {
    double speed;

    public double getSpeedMPH() {
        return speed * SpeedConverter.KILOMETERS_TO_MILES.getConverter();
    }
    @Override
    public double getSpeed() {
        return getSpeedMPH();
    }
}
