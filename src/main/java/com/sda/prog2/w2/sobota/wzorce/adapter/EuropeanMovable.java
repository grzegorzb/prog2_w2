package com.sda.prog2.w2.sobota.wzorce.adapter;

public interface EuropeanMovable {
    double getSpeed();
}
