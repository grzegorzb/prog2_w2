package com.sda.prog2.w2.sobota.wzorce.command;

@FunctionalInterface
public interface FileOperation {
String performOperation(String content);
}