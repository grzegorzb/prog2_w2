package com.sda.prog2.w2.sobota.wzorce.command;

public class MyFile {
    private String fileName;
    private String content;


    public MyFile(String fileName, String content) {
        this.fileName = fileName;
//        this.content = content;
    }

    public String createFile(String content) {
        this.content=content;
        return "Create file "+fileName+" with content "+content;
    }

    public String updateFile(String content) {
        this.content+=content;
        return "Update file "+fileName+" with new content "+content;
    }

    public String getFileName() {
        return fileName;
    }
    public String getContent() {
        return content;
    }


}
