package com.sda.prog2.w2.sobota.wzorce.command;

public class UpdateFileOperation implements FileOperation {
    private MyFile myFile;

    public UpdateFileOperation(MyFile myFile) {
        this.myFile = myFile;
    }

    @Override
    public String performOperation(String content) {
        return myFile.updateFile(content);
    }
}
