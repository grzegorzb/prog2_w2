package com.sda.prog2.w2.sobota.wzorce.flyweight;

public class FordMustang {
    private FordMustangBase mustangBase;
    private String radio;

    public FordMustang(String color, String equipment, String radio) {
        System.out.println(this.getClass()+" constructor");
        this.mustangBase = MustangBaseFactory.getMustangBase(color,equipment);
        this.radio = radio;
    }
}
