package com.sda.prog2.w2.sobota.wzorce.prototype;

import java.util.List;

public class PrototypeDemo {
    public static void main(String[] args) {
        ShoppingList shoppingList = new ShoppingList();
        shoppingList.loadData();

        try {
            ShoppingList zakupy1 = (ShoppingList) shoppingList.clone();
            List<String> listaZakupow = zakupy1.getShopList();
            listaZakupow.add("proszek do prania");
            listaZakupow.remove("chleb");
        }catch(CloneNotSupportedException e){
            e.printStackTrace();
        }
    }
}
