package com.sda.prog2.w2.sobota.wzorce.prototype;

import java.util.ArrayList;
import java.util.List;

public class ShoppingList implements Cloneable {
    private List<String> shopList;

    public ShoppingList(){ shopList = new ArrayList<>();
    }

    public ShoppingList(List<String> list){this.shopList=list;}

    public void loadData(){
        shopList.add("ser");
        shopList.add("pomidory");
        shopList.add("chleb");
    }

    public List<String> getShopList(){return this.shopList;}

    @Override
    public Object clone() throws CloneNotSupportedException{
        List<String> temp = new ArrayList<>();
        for (String s : this.getShopList()){
            temp.add(s);
        }
        return new ShoppingList(temp);
    }
}
