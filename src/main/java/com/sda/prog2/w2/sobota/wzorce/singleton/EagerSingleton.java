package com.sda.prog2.w2.sobota.wzorce.singleton;

public class EagerSingleton {
    private static final EagerSingleton instan = new EagerSingleton();

    private EagerSingleton() {
        System.out.println("Eager Singleton installation");
    }
    public static EagerSingleton getInstance(){
        System.out.println("Get instance Eager");
        return instan;
    }
}
