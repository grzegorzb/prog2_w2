package com.sda.prog2.w2.sobota.wzorce.singleton;

public class LazySigleton {
    private static LazySigleton instan; //not final

    private LazySigleton() {
        System.out.println("Lazy Singleton initialization");
    }

    public static LazySigleton getInstance() {
        if (instan == null) {
            instan = new LazySigleton();
        }
        return instan;
    }

    public String getSth() {
        return "Something";
    }
}
