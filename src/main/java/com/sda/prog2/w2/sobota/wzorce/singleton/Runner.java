package com.sda.prog2.w2.sobota.wzorce.singleton;

public class Runner {
    public static void main(String[] args) {
        EagerSingleton.getInstance();
        EagerSingleton.getInstance();

        LazySigleton.getInstance();
        LazySigleton.getInstance();

//        System.out.println((EagerSingleton.getInstance());
    }
}
