package com.sda.prog2.w3.serializacja.jaxb.xml;

import com.sda.prog2.w3.serializacja.Person;

import java.util.Optional;

public class XmlConverterTest {
    public static void main(String[] args) {

        Person person = new Person("Anna", "Nowak", 22, "K", 1, "PL");

        PersonToXmlConverter.personToXmlString(person);

        PersonToXmlConverter.personToXmlFile(person);

        Optional<Person> optionalPerson = PersonToXmlConverter.xmlFileToPerson("personToXml.xml");
        if (optionalPerson.isPresent()) {
            System.out.println(optionalPerson.get());
        } else System.out.println("Nobody");
    }
}